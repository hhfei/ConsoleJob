﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;
using System.ComponentModel;
using System.Configuration;

namespace ExhibitionSystem.Job.Code
{
    public abstract class SqlHelper
    {
        static string ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
        static SqlHelper()
        {
            
        }

        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="SQLString">SQL语句</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string SQLString, params SqlParameter[] cmdParms)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        PreparCommand(connection, cmd, SQLString, CommandType.Text, cmdParms);
                        int rows = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        return rows;
                    }
                    catch (System.Data.SqlClient.SqlException e)
                    {
                        throw e;
                    }
                }
            }
        }

        /// <summary>
        /// 执行sql语句或存储过程 返回ExecuteScalar （返回自增的ID）不带参数
        /// </summary>
        /// <param name="SQLString">sql语句或存储过程名称</param>
        /// <param name="parms">参数</param>
        /// <returns></returns>
        public static object ExecuteScalar(string SQLString, params DbParameter[] parms)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        PreparCommand(connection, cmd, SQLString, CommandType.Text, parms);
                        object result = cmd.ExecuteScalar();
                        cmd.Parameters.Clear();
                        return result;
                    }
                    catch (System.Data.SqlClient.SqlException e)
                    {
                        throw e;
                    }
                }
            }
        }

        /// <summary>
        /// 执行sql语句或存储过程，返回DataSet不带参数
        /// </summary>
        /// <param name="SQLString">sql语句或存储过程名称</param>
        /// <param name="parms">参数</param>
        /// <returns></returns>
        public static DataSet ExecuteDataSet(string SQLString, params DbParameter[] parms)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        PreparCommand(connection, cmd, SQLString, CommandType.Text, parms);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            DataSet ds = new DataSet();
                            da.Fill(ds);
                            if (cmd.Parameters != null)
                                cmd.Parameters.Clear();
                            return ds;
                        }
                    }
                    catch (System.Data.SqlClient.SqlException e)
                    {
                        throw e;
                    }
                }
            }
        }

        #region ExecuteList Entity 执行sql语句或者存储过程，返回一个List<T>---List<T>
        public static List<Entity> ExecuteList<Entity>(string commandTextOrSpName, params DbParameter[] parms) where Entity : class
        {
            return GetListFromDataSet<Entity>(ExecuteDataSet(commandTextOrSpName, parms));
        }
        
        #endregion

        #region ExecuteEntity 执行sql语句或者存储过程，返回一个Entity---Entity
      
        public static Entity ExecuteEntity<Entity>(string commandTextOrSpName, CommandType commandType, params DbParameter[] parms) where Entity : class
        {
            return GetEntityFromDataSet<Entity>(ExecuteDataSet(commandTextOrSpName, parms));
        }
      
        #endregion

        #region ---PreparCommand 构建一个通用的command对象供内部方法进行调用---
        /// <summary>
        /// 不带参数的设置sqlcommand对象
        /// </summary>
        /// <param name="conn">sqlconnection对象</param>
        /// <param name="cmd">sqlcommmand对象</param>
        /// <param name="commandTextOrSpName">sql语句或存储过程名称</param>
        /// <param name="commandType">语句的类型</param>
        private static void PreparCommand(DbConnection conn, DbCommand cmd, string commandTextOrSpName, CommandType commandType)
        {
            //打开连接
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }

            //设置SqlCommand对象的属性值
            cmd.Connection = conn;
            cmd.CommandType = commandType;
            cmd.CommandText = commandTextOrSpName;
            cmd.CommandTimeout = 60;
        }
        /// <summary>
        /// 设置一个等待执行的SqlCommand对象
        /// </summary>
        /// <param name="conn">sqlconnection对象</param>
        /// <param name="cmd">sqlcommmand对象</param>
        /// <param name="commandTextOrSpName">sql语句或存储过程名称</param>
        /// <param name="commandType">语句的类型</param>
        /// <param name="parms">参数，sqlparameter类型，需要指出所有的参数名称</param>
        private static void PreparCommand(DbConnection conn, DbCommand cmd, string commandTextOrSpName, CommandType commandType, params SqlParameter[] parms)
        {
            //打开连接
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }

            //设置SqlCommand对象的属性值
            cmd.Connection = conn;
            cmd.CommandType = commandType;
            cmd.CommandText = commandTextOrSpName;
            cmd.CommandTimeout = 60;

            if (parms != null)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.AddRange(parms);
            }
        }
        /// <summary>
        /// PreparCommand方法，可变参数为object需要严格按照参数顺序传参
        /// 之所以会用object参数方法是为了我们能更方便的调用存储过程，不必去关系存储过程参数名是什么，知道它的参数顺序就可以了 sqlparameter必须指定每一个参数名称
        /// </summary>
        /// <param name="conn">sqlconnection对象</param>
        /// <param name="cmd">sqlcommmand对象</param>
        /// <param name="commandTextOrSpName">sql语句或存储过程名称</param>
        /// <param name="commandType">语句的类型</param>
        /// <param name="parms">参数，object类型，需要按顺序赋值</param>
        private static void PreparCommand(DbConnection conn, DbCommand cmd, string commandTextOrSpName, CommandType commandType, params object[] parms)
        {
            //打开连接
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }

            //设置SqlCommand对象的属性值
            cmd.Connection = conn;
            cmd.CommandType = commandType;
            cmd.CommandText = commandTextOrSpName;
            cmd.CommandTimeout = 60;

            cmd.Parameters.Clear();
            if (parms != null)
            {
                cmd.Parameters.AddRange(parms);
            }
        }
        #endregion

        #region 通过Model反射返回结果集 Model为 Entity 泛型变量的真实类型---反射返回结果集
        /// <summary>
        /// 反射返回一个List T 类型的结果集
        /// </summary>
        /// <typeparam name="T">Model中对象类型</typeparam>
        /// <param name="ds">DataSet结果集</param>
        /// <returns></returns>
        public static List<Entity> GetListFromDataSet<Entity>(DataSet ds) where Entity : class
        {
            List<Entity> list = new List<Entity>();//实例化一个list对象
            PropertyInfo[] propertyInfos = typeof(Entity).GetProperties();     //获取T对象的所有公共属性

            DataTable dt = ds.Tables[0];    // 获取到ds的dt
            if (dt.Rows.Count > 0)
            {
                //判断读取的行是否>0 即数据库数据已被读取
                foreach (DataRow row in dt.Rows)
                {
                    Entity model1 = System.Activator.CreateInstance<Entity>();//实例化一个对象，便于往list里填充数据
                    foreach (PropertyInfo propertyInfo in propertyInfos)
                    {
                        try
                        {
                            //遍历模型里所有的字段
                            if (row[propertyInfo.Name] != System.DBNull.Value)
                            {
                                //判断值是否为空，如果空赋值为null见else
                                if (propertyInfo.PropertyType.IsGenericType && propertyInfo.PropertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                                {
                                    //如果convertsionType为nullable类，声明一个NullableConverter类，该类提供从Nullable类到基础基元类型的转换
                                    NullableConverter nullableConverter = new NullableConverter(propertyInfo.PropertyType);
                                    //将convertsionType转换为nullable对的基础基元类型
                                    propertyInfo.SetValue(model1, Convert.ChangeType(row[propertyInfo.Name], nullableConverter.UnderlyingType), null);
                                }
                                else
                                {
                                    propertyInfo.SetValue(model1, Convert.ChangeType(row[propertyInfo.Name], propertyInfo.PropertyType), null);
                                }
                            }
                            else
                            {
                                propertyInfo.SetValue(model1, null, null);//如果数据库的值为空，则赋值为null
                            }
                        }
                        catch (Exception)
                        {
                            propertyInfo.SetValue(model1, null, null);//如果数据库的值为空，则赋值为null
                        }
                    }
                    list.Add(model1);//将对象填充到list中
                }
            }
            return list;
        }
        /// <summary>
        /// 反射返回一个T类型的结果
        /// </summary>
        /// <typeparam name="T">Model中对象类型</typeparam>
        /// <param name="reader">SqlDataReader结果集</param>
        /// <returns></returns>
        public static Entity GetEntityFromDataReader<Entity>(DbDataReader reader) where Entity : class
        {
            Entity model = System.Activator.CreateInstance<Entity>();           //实例化一个T类型对象
            PropertyInfo[] propertyInfos = model.GetType().GetProperties();     //获取T对象的所有公共属性
            using (reader)
            {
                if (reader.Read())
                {
                    foreach (PropertyInfo propertyInfo in propertyInfos)
                    {
                        //遍历模型里所有的字段
                        if (reader[propertyInfo.Name] != System.DBNull.Value)
                        {
                            //判断值是否为空，如果空赋值为null见else
                            if (propertyInfo.PropertyType.IsGenericType && propertyInfo.PropertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                            {
                                //如果convertsionType为nullable类，声明一个NullableConverter类，该类提供从Nullable类到基础基元类型的转换
                                NullableConverter nullableConverter = new NullableConverter(propertyInfo.PropertyType);
                                //将convertsionType转换为nullable对的基础基元类型
                                propertyInfo.SetValue(model, Convert.ChangeType(reader[propertyInfo.Name], nullableConverter.UnderlyingType), null);
                            }
                            else
                            {
                                propertyInfo.SetValue(model, Convert.ChangeType(reader[propertyInfo.Name], propertyInfo.PropertyType), null);
                            }
                        }
                        else
                        {
                            propertyInfo.SetValue(model, null, null);//如果数据库的值为空，则赋值为null
                        }
                    }
                    return model;//返回T类型的赋值后的对象 model
                }
            }
            return default(Entity);//返回引用类型和值类型的默认值0或null
        }
        /// <summary>
        /// 反射返回一个T类型的结果
        /// </summary>
        /// <typeparam name="T">Model中对象类型</typeparam>
        /// <param name="ds">DataSet结果集</param>
        /// <returns></returns>
        public static Entity GetEntityFromDataSet<Entity>(DataSet ds) where Entity : class
        {
            return GetListFromDataSet<Entity>(ds).FirstOrDefault();
        }
        #endregion

    }
}

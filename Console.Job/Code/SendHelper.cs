﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ExhibitionSystem.Job.Code
{
    public class SendHelper
    {
        static string sendsms_url = ConfigurationManager.AppSettings["sendsms_url"];

        /// <summary>
        /// 发送短信
        /// </summary>
        /// <param name="targerAccount"></param>
        /// <param name="content"></param>
        /// <param name="sendString"></param>
        /// <param name="resultString"></param>
        /// <returns></returns>
        public static bool SendSMS(string targerAccount,string content,ref string sendString, ref string resultString)
        {
            resultString = string.Empty;
            sendString = "{"
                + "\"FromSys\":\"E89\","
                + $"\"DealID\":\"{Guid.NewGuid()}\","
                + $"\"Target\":\"{targerAccount}\","
                + $"\"Content\":\"{content}\","
                + $"\"SignCode\":\"\","
                + $"\"SendTime\":\"{DateTime.Now}\","
                + $"\"TargetType\":\"account\""
                + "}";
            try
            {
                resultString = NetHelper.Post(sendsms_url, sendString);
                JavaScriptSerializer js = new JavaScriptSerializer();
                Model.SmsResult result = js.Deserialize<Model.SmsResult>(resultString);
                if(!result.IsSuccess)
                {
                    Logger.Error("SendSMS【接口返回错误】", result.Describe);
                }
                return result.IsSuccess;
            }
            catch (Exception ex)
            {
                Logger.Error("SendSMS【调用错误】", ex.ToString());
                return false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ExhibitionSystem.Job.Code
{
    public class NetHelper
    {
        public static string Post(string url, string postData)
        {
            try
            {
                var client = new WebClient();
                // 构造POST参数
                byte[] sendData = Encoding.UTF8.GetBytes(postData);
                client.Headers.Add("Content-Type", "application/json");
                client.Headers.Add("ContentLength", sendData.Length.ToString());
                var result = client.UploadData(url, "POST", sendData);
                return Encoding.UTF8.GetString(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

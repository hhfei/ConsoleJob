﻿using ExhibitionSystem.Job.DataAccess;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExhibitionSystem.Job
{
    /// <summary>
    /// 任务-发送满意度评价
    /// </summary>
    public class SendSatisfactionEvaluation
    {
        const string SmsType = "满意度评价提醒";
        const string SmsContent = "参观已结束，欢迎您点击链接https://www.wjx.cn/m/80822245.aspx为展厅参观接待提出宝贵的建议，帮助展厅运营人员不断优化展厅参观接待，非常感谢";
        public static void Run()
        {
            try
            {
                Console.WriteLine("# 开始执行[发送参观满意度评分提醒]任务");
                var list = VisitDA.Single.GetVisits();
                Console.WriteLine($"#\t获取到{list.Count}条数据需要发送提醒的预约");
                foreach (var item in list)
                {
                    string sendString = string.Empty;
                    string resultString = string.Empty;
                    DateTime sendTime = DateTime.Now;
                    bool sendResult = Code.SendHelper.SendSMS(item.CreateUserId, SmsContent, ref sendString, ref resultString);
                    if (sendResult)
                    {
                        Model.UserSendMsg sendLog = new Model.UserSendMsg()
                        {
                            UserId = item.CreateUserId,
                            Mobile = item.Id.ToString(),
                            MsgContent = SmsContent,
                            SmsType = SmsType,
                            Senddate = sendTime.ToString(),
                            SendModel = sendString,
                            SendResult = resultString,
                            CreateTime = DateTime.Now
                        };
                        UserSendMsgDA.Single.Add(sendLog);
                        Console.WriteLine($"#\t发送成功：账号：{item.CreateUserId}");
                    }
                }
                Console.WriteLine("# 执行结束");
            }
            catch (Exception ex)
            {
                Code.Logger.Error("SendSatisfactionEvaluation.Run", ex.ToString());
                throw ex;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExhibitionSystem.Job.Model
{
    public partial class UserSendMsg
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Mobile { get; set; }
        public string MsgContent { get; set; }
        public string SmsType { get; set; }
        public string Senddate { get; set; }
        public string SendModel { get; set; }
        public string SendResult { get; set; }
        public System.DateTime CreateTime { get; set; }
    }
}

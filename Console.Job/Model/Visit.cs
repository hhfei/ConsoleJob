﻿using System;


namespace ExhibitionSystem.Job.Model
{
    public class Visit
    {
        public int Id { get; set; }
        public System.DateTime CreateTime { get; set; }
        public string CreateUserId { get; set; }
        public string Phone { get; set; }
        public string VisitTitle { get; set; }
        public System.DateTime VisitTime { get; set; }
        public System.DateTime VisitStartTime { get; set; }
        public System.DateTime VisitEndTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExhibitionSystem.Job.Model
{
    public class SmsModel
    {
        public string FromSys { get { return "E89"; } }
        public string DealID { get { return Guid.NewGuid().ToString(); } }
        public string Target { get; set; }
        public string Content { get; set; }
        public string SignCode { get; set; }
        public string SendTime { get; set; }
        public string TargetType { get { return "account"; } }

    }
}

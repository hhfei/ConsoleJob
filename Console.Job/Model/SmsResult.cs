﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExhibitionSystem.Job.Model
{
    public class SmsResult
    {
        public string DealID { get; set; }
        public string Describe { get; set; }
        public bool IsSuccess { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExhibitionSystem.Job
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("----------------展厅预约任务程序----------------");
                Console.WriteLine("    实现功能如下：");
                Console.WriteLine("        1、展厅展览结束发送参观满意度评分提醒，参数为sms");
                Console.ReadKey();
            }
            else
            {
                if (args[0].ToLower().Equals("sms"))
                {
                    SendSatisfactionEvaluation.Run();
                    Console.ReadKey();
                }
            }
        }
    }
}

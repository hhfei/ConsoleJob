﻿using ExhibitionSystem.Job.Model;
using System.Collections.Generic;

namespace ExhibitionSystem.Job.DataAccess
{
    public class VisitDA
    {
        private static VisitDA _single = new VisitDA();
        public static VisitDA Single
        {
            get { return _single; }
        }

        // 获取任务
        string sql = @"SELECT Id,CreateTime,CreateUserId,Phone,VisitTime,VisitStartTime,VisitEndTime
                          FROM ExhibitionSystem.dbo.Visit v where IsDelete = 0 and VisitEndTime > '2020/05/15' and VisitEndTime<getdate() and IsDelete = 0
                          and not exists(select 1 from UserSendMsg m where cast(m.Mobile as varchar) = cast(v.Id as varchar) and m.SmsType = '满意度评价提醒')";

        /// <summary>
        /// 获取需要发送满意度评价提醒的展厅数据集合
        /// </summary>
        /// <returns></returns>
        public List<Visit> GetVisits()
        {
            try
            {
                return Code.SqlHelper.ExecuteList<Visit>(sql);
            }
            catch (System.Exception ex)
            {
                Code.Logger.Error("VisitDA.GetVisits",ex.ToString());
                throw ex;
            }
        }

    }
}

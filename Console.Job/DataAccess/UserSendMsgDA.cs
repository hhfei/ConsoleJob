﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExhibitionSystem.Job.DataAccess
{
    public class UserSendMsgDA
    {
        private static UserSendMsgDA _single = new UserSendMsgDA();
        public static UserSendMsgDA Single
        {
            get { return _single; }
        }

        public bool Add(Model.UserSendMsg model)
        {
            try
            {
                string insertSql = "insert into UserSendMsg(UserId,Mobile,MsgContent,SmsType,Senddate,SendModel,SendResult,CreateTime) values(@UserId,@Mobile,@MsgContent,@SmsType,@Senddate,@SendModel,@SendResult,@CreateTime)";
                SqlParameter[] parms = {
                            new SqlParameter("@UserId",model.UserId),
                            new SqlParameter("@Mobile",model.Mobile),
                            new SqlParameter("@MsgContent",model.MsgContent),
                            new SqlParameter("@SmsType",model.SmsType),
                            new SqlParameter("@Senddate",model.Senddate),
                            new SqlParameter("@SendModel",model.SendModel),
                            new SqlParameter("@SendResult",model.SendResult),
                            new SqlParameter("@CreateTime",model.CreateTime)
                        };
                int result = Code.SqlHelper.ExecuteSql(insertSql, parms);
                return result == 1;
            }
            catch (Exception ex)
            {
                Code.Logger.Error("UserSendMsgDa.Add", ex.ToString());
                throw ex;
            }
            
        }
    }
}
